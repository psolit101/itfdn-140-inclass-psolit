myguess=3

echo "Welcome to the number guessing game!"
read -sp "Enter your guess between 1-5: " yourguess

if [ "$myguess" != "$yourguess" ]; then
    echo "$yourguess was not right"
    echo "my number was $myguess"
else
    echo "$yourguess was right"
fi
