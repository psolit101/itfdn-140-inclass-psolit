# !/bin/sh
# Title: recall
# Author: 
# Date: 00/00/0000
# Purpose: recall tips and notes from store database file
# Update: 

# consider adding an if statment to print out usage details if the user passes in a "-h"
# consider adding a log file that includes who ran the script and what arguments that passed in.

# variables
store_data="$HOME/scripts/shell/store.db"


# start logic here
if [ "$#" -eq 0 ]; then
   echo "Enter note then quit using Ctrl+D"
   # note - following cat indicates input from keyboard, also this can be blank also.
   cat - >> $store_data
   date >> $store_data
   echo "User: " >> $store_data
   whoami >> $store_data

else
   echo "$@ >> $store_data"
fi

if [ "$1" = "-h" ] ;then
   echo "Usage: $0 then input your notes. Use ./recall.sh -bu to backup/copy store.db file to your home directory"
   date
   echo " Last User: " 
   whoami
fi

if [ "$1" = "-bu" ] ;then
   echo "Backing up store.db file to home directory"
  cp ~/scripts/shell/store.db ~
  echo "back up finished"
fi

   exit 0


