# !/bin/bash
# Author: Paul Soli
# Title: security_check.sh
# Date: 05/19/2022
# Purpose: Generate a report of all folders that allow Read, Write and Execute for the group "Other".
# Update: Consider adding the server name and logged in users to the 

## Define a variable for date, note you must surround the date command in back tics to capture the command output.  
## If you have a quote it then you will only see the word "date" and not the actual date and time.
now=`date`

#add log variable
sclog=security_check.log

## Create a header at the top of the security log

echo "*******************************" >> $sclog
echo "**** Start security report ******" >> $sclog

## add hostname to the security log
echo "*** Security report for: $HOSTNAME " >> $sclog

# add date to the security log
echo "*** Report Date: $now " >> $sclog

## Log who was logged into the system when the script was run.
echo "*** Found the following users logged in when this script was run" >> $sclog
who >> $sclog

## Add find command here and redirect the output to $sclog
# Consider that you need find 
# - Only search for folders and not files
# - Only search for folder that grant the group "other" read, write and execute "rwx"
# - You can ignore any content that denies your script access as this
# - must not allow "other" read,write and execute permissions by default

find / -perm -757 -type d 2>/dev/null

cat security_check.log | tail -n 5

echo "IP & device source:"
ip -4 -o addr show | grep 2: | awk '{print $4}' | cut -d/ -f1

echo "input password for device type (ctrl c to exit)"

cat /etc/sysconfig/network-scripts/ifcfg-ens192 | grep DEVICE

## add two blank lines
echo >> $sclog
echo >> $sclog
## close log for this run
echo "**** End security report ******" >>$sclog
echo "*******************************" >>$sclog
