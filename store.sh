# !/bin/sh
# Title: store
# Author: 
# Date: 00/00/0000
# Purpose: store tips and trips for later retrieval using recall script.
# Update: 

# consider adding an if statment to print out usage details if the user passes in a "-h"
# consdier adding a message to the default output that tell the user everything is being printed
# because no argument was passed in.
# consider adding a log file that includes who ran the script and what arguments that passed in.

# variables
store_data="$HOME/scripts/store.db"

# start logic here
if [ "$#" -eq 0 ]; then
   echo "Enter note then quit using Ctrl+D"
   # note - following cat indicates input from keyboard, also this can be blank also.
   cat - >> $store_data
else
   echo "$@ >> $store_data"
fi

if [ "$1" = "-h" ] ;then
   echo "Usage: $0 then input your notes"
   date
   echo " Last User: " 
   whoami
   echo " Back up store.db file with copy command:"
   echo "cp ~/scripts/store.db <destination>"
   exit 0
fi

