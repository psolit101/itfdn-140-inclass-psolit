#!/bin/bash
# Title: <script_name>.sh
# Date: 00/00/2019
# Author:
# Purpose: Default script template
# Update

# first argument is the name, we hope.
name=$1
# say hello
echo "Hello $name, pleased to meet you."
mylog="hello.log"
echo "adding args to the log"
echo "$@">>$mylog

if [ “$1” = “night” ] # testing for the string "night" lowercase only if we match
 then
  echo "Good evening"
  exit 0
fi
# default answer if no argument found
echo "Good Day"

#end
