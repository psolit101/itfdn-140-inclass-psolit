#!/bin/bash
# Title: arguments.sh
# Author:
# Date:
# Purpose
# Update:

if [ "$#" -ne 3 ]; then
    echo "No arguments detected, please enter: city name, the day(i.e. Wednesday)  and the month (i.e. June)."
    echo "exiting now"
    exit 0
fi

echo "The script name is: $1"
echo "The total arguments provided was: $0"
echo "The day provided was: $*"
echo "All the arguments provided were: $2"
